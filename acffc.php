<?php
/**
	ACFFC -  Advanced Custom Fields Field Creator 
	----------------------------------------------
	Version: 1.0
	Contributors: Jamie Telin (jamie.telin@gmail.com)
	Description: 
					ACFFC is a "api" layer to ACF for creating new field groups and fields though a theme.
					It gives the developer a more structured OOP approach to creating fields.
					It auto completes any missing variables, allowing developer to quickly add new fields for their themes.
					It also automaticly generates ID and saves them in options for cacheing.
	Usage example:
					$ACFFC->add_field_group( 'product_page',  'Product page');
					$ACFFC->add_text('product_page', array(
								'label'  => 'Title' ,
								'name'  => 'title' ,
								'formatting'  => 'none' ,
								));
					$ACFFC->add_textarea('product_page', array(
								'label'  => 'Short Content' ,
								'name'  => 'short_content' ,
								'instructions'  => 'Enter a short version of content.' ,
								));
					$ACFFC->add_wysiwyg('product_page', array(
								'label'  => 'Content' ,
								'name'  => 'content' ,
								'formatting'  => 'none' ,
								));
					$ACFFC->add_image('product_page', array(
								'label'  => 'Image' ,
								'name'  => 'image' ,
								));
					$ACFFC->add_rule('product_page', array(
								'param'  => 'page_template' ,
								'operator'  => '==' ,
								'value'  => 'tpl-productpage.php' ,
								));
					$ACFFC->add_hide_on_screen('product_page', array(
								'the_content', 'custom_fields', 'featured_image'
								));
**/
class ACFFC
{
	public $field_groups;
	
	private $ids;
	private $options;
	private $active;
	private $field_nr;
	
	function __construct($options = array()) 
	{
		$this->field_nr = 0;
		$this->field_groups = array();
		$this->ids = array();
		$this->options = array();
		$this->options = array_merge($options, $this->options);
		$this->active = false;
		
		$this->from_db();
	}
	function __destruct() 
	{
		$this->to_db();
	}
	/**
	* from_db()
	* @desc Fetches data about field ids from db using WordPress option api
	* @return boolean
	**/
	public function from_db()
	{
		//Fetches, unserializes and inserts id, from db using WordPress options, into an array
		$this->ids = unserialize(get_option('dmon_acf_ids'));
		if(!$this->ids)
		{
			//...returns false if no id was found
			return false;
		}
		//...returns true if id existed
		return true;
	}
	/**
	* to_db()
	* @desc Saves data about field ids from db using WordPress option api
	* @return boolean
	**/
	public function to_db()
	{
		//Serializes id array and saved to db using WordPress options
		update_option('dmon_acf_ids', serialize($this->ids));
		return true;
	}
	/**
	* get_field_group_names()
	* @desc Returns an array of field group names
	* @return array()
	**/
	public function get_field_group_names()
	{
		$array = array();
		$i = 0;
		foreach($this->field_groups as $key => $value)
		{
			$array[$i] = $key;
			$i++;
		}
		return $array;
	}
	/**
	* the_field_group_names()
	* @desc Displays a unorderd list of field group names
	**/
	public function the_field_group_names()
	{
		echo '<ul>';
		foreach($this->field_groups as $key => $value)
		{
			echo '<li>'.$key.'</li>';

		}
		echo '</ul>';
	}
	/**
	* dump()
	* @desc Displays all data from field groups array
	**/
	public function dump()
	{
		echo '<pre>';
		var_dump($this->field_groups);
		echo '</pre>';
	}
	/**
	* register()
	* @desc Registers field groups and fields in ACF
	**/
	public function register()
	{
		//Make sure plugin is installed and active
		if(function_exists('register_field_group'))
		{
			//Register all fields in the loop
			foreach($this->field_groups as $key => $value)
			{
				register_field_group($this->field_groups[$key]);
			}
		}
	}
	/**
	* sanitize_lable()
	* @desc Controlls lable string and automaticly generates one if its missing
	* @param string
	* @return string
	**/
	public function sanitize_lable($lable = false)
	{
		if(!$lable)
		{
			$lable = 'Field ' . $this->field_nr;
			$this->field_nr++;
		}
		return $lable;
	}
	/**
	* sanitize_name()
	* @desc Controlls name and cleans up any unallowed characters, if missing it uses lable
	* @param string
	* @param string
	* @return string
	**/
	public function sanitize_name($name = false, $lable)
	{
		//Generate a character list for updating unallowed chacters
		$normalizeChars = array( 
            'Á'=>'A', 'À'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Å'=>'A', 'Ä'=>'A', 'Æ'=>'AE', 'Ç'=>'C', 
            'É'=>'E', 'È'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Í'=>'I', 'Ì'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ð'=>'Eth', 
            'Ñ'=>'N', 'Ó'=>'O', 'Ò'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 
            'Ú'=>'U', 'Ù'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 
    
            'á'=>'a', 'à'=>'a', 'â'=>'a', 'ã'=>'a', 'å'=>'a', 'ä'=>'a', 'æ'=>'ae', 'ç'=>'c', 
            'é'=>'e', 'è'=>'e', 'ê'=>'e', 'ë'=>'e', 'í'=>'i', 'ì'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'eth', 
            'ñ'=>'n', 'ó'=>'o', 'ò'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 
            'ú'=>'u', 'ù'=>'u', 'û'=>'u', 'ü'=>'u', 'ý'=>'y', 
            
            'ß'=>'sz', 'þ'=>'thorn', 'ÿ'=>'y',
			
			'-'=>'_', ' '=>'_', '"'=>'', '\''=>'', '.'=>'', ','=>'', ':'=>'', ';'=>'', '?'=>'', '+'=>'plus'
        );
		//If name is not set it will be automatic generated from lable
		if(!$name)
		{
			$name = strtolower(strtr($lable, $normalizeChars));
			do{
				$name = str_replace('__', '_', $name);
			} while(strpos($name, '__') !== false);
		}
		return $name;
	}
	/**
	* get_default_field()
	* @desc Returns an array of default data for creatinga field
	* @param array()
	* @return array()
	**/
	public function get_default_field($args = array())
	{
		$this->active .= '_'.$args['name'];
		
		$array = array (
		  'label'  => $this->sanitize_lable($args['label']) ,
		  'name'  => $this->sanitize_name($args['name'], $this->sanitize_lable($args['label'])) ,
		  'instructions'  => '' ,
		  'required'  => '0' ,
		  'default_value'  => '' ,
		  'key'  => $this->get_new_key() ,
		  'order_no'  => '0' ,
		);
		
		$array = array_merge($array, $args);

		return $array;
	}
	/**
	* get_default_text_field()
	* @desc Returns an array of default data for creating a text field
	* @param array()
	* @return array()
	**/
	public function get_default_text_field($args = array())
	{
		$this->active .= '_'.$args['name'];
		
		$array = array (
		  'label'  => $this->sanitize_lable($args['label']) ,
		  'name'  => $this->sanitize_name($args['name'], $this->sanitize_lable($args['label'])) ,
		  'type'  => 'text' ,
		  'instructions'  => '' ,
		  'required'  => '0' ,
		  'default_value'  => '' ,
		  'formatting'  => 'none' , // html, none
		  'key'  => $this->get_new_key() ,
		  'order_no'  => '0' ,
		);
		
		$array = array_merge($array, $args);

		return $array;
	}
	/**
	* get_default_wysiwyg_field()
	* @desc Returns an array of default data for creating a wysiwyg field
	* @param array()
	* @return array()
	**/
	public function get_default_wysiwyg_field($args = array())
	{
		$this->active .= '_'.$args['name'];
		
		$array = array (
			'label'  => $this->sanitize_lable($args['label']) ,
			'name'  => $this->sanitize_name($args['name'], $this->sanitize_lable($args['label'])) ,
			'type'  => 'wysiwyg' ,
			'instructions'  => '' ,
			'required'  => '0' ,
			'default_value'  => '' ,
			'the_content' => 'yes', // yes, no
			'media_upload' => 'yes', // yes, no
			'toolbar'  => 'full' , // full, basic
			'key'  => $this->get_new_key() ,
			'order_no'  => '0' ,
		);
		
		$array = array_merge($array, $args);
		
		return $array;
	}
	/**
	* get_default_image_field()
	* @desc Returns an array of default data for creating a image field
	* @param array()
	* @return array()
	**/
	public function get_default_image_field($args = array())
	{
		$this->active .= '_'.$args['name'];
		
		$array = array (
			'label'  => $this->sanitize_lable($args['label']) ,
			'name'  => $this->sanitize_name($args['name'], $this->sanitize_lable($args['label'])) ,
			'type'  => 'image' ,
			'preview_image'  => 'large' , // thumbnail
			'save_format'  => 'object' , // object, url, id
			'instructions'  => '' ,
			'required'  => '0' ,
			'key'  => $this->get_new_key() ,
			'order_no'  => '0' ,
		);
		
		$array = array_merge($array, $args);
		
		return $array;
	}
	/**
	* get_default_textarea_field()
	* @desc Returns an array of default data for creating a textarea field
	* @param array()
	* @return array()
	**/
	public function get_default_textarea_field($args = array())
	{
		$this->active .= '_'.$args['name'];
		
		$array = array (
			'label'  => $this->sanitize_lable($args['label']) ,
			'name'  => $this->sanitize_name($args['name'], $this->sanitize_lable($args['label'])) ,
			'type'  => 'textarea' ,
			'instructions'  => '' ,
			'required'  => '0' ,
			'default_value'  => '' ,
			'formatting'  => 'br' , // html, none, br
			'key'  => $this->get_new_key() ,
			'order_no'  => '0' ,
		);
		
		$array = array_merge($array, $args);
		
		return $array;
	}
	/**
	* get_default_pagelink_field()
	* @desc Returns an array of default data for creating a page link field
	* @param array()
	* @return array()
	**/
	public function get_default_pagelink_field($args = array())
	{
		$this->active .= '_'.$args['name'];
		
		$array = array (
			'label'  => $this->sanitize_lable($args['label']) ,
			'name'  => $this->sanitize_name($args['name'], $this->sanitize_lable($args['label'])) ,
			'type'  => 'page_link' ,
			'instructions'  => '' ,
			'required'  => '0' ,
			'default_value'  => '' ,
			'allow_null' => 0,
			'multiple' => 0,
			'post_type' => 'page', // post, page, attachment, slide, wpcf7_contact_form
			'formatting'  => 'br' , // html, none, br
			'key'  => $this->get_new_key() ,
			'order_no'  => '0' ,
		);
		
		$array = array_merge($array, $args);
		
		return $array;
	}
	/**
	* get_default_select_field()
	* @desc Returns an array of default data for creating a select field
	* @param array()
	* @return array()
	**/
	public function get_default_select_field($args = array())
	{
		$this->active .= '_'.$args['name'];
		
		$array = array (
			'label'  => $this->sanitize_lable($args['label']) ,
			'name'  => $this->sanitize_name($args['name'], $this->sanitize_lable($args['label'])) ,
			'type'  => 'select' ,
			'instructions'  => '' ,
			'required'  => '0' ,
			'default_value'  => '' ,
			'allow_null' => 0,
			'multiple' => 0,
			'choices'  => '' , // separate by comma but will transformed to newlines \n
			'key'  => $this->get_new_key() ,
			'order_no'  => '0' ,
		);
		
		$array = array_merge($array, $args);
		
		if(!empty($array['choices']))
		{
			if(!is_array($array['choices']))
			{
				$tmp = explode(',', $array['choices']);
			}
			else 
			{
				$tmp = $array['choices'];
			}
			$array['choices'] = '';
			foreach($tmp as $choices)
			{
				$array['choices'] .= trim($choices)."\n";
			}
			
		}
		
		return $array;
	}
	/**
	* get_default_colorpicker_field()
	* @desc Returns an array of default data for creating a color picker field
	* @param array()
	* @return array()
	**/
	public function get_default_colorpicker_field($args = array())
	{
		$this->active .= '_'.$args['name'];
		
		$array = array (
		  'label'  => $this->sanitize_lable($args['label']) ,
		  'name'  => $this->sanitize_name($args['name'], $this->sanitize_lable($args['label'])) ,
		  'type'  => 'color_picker' ,
		  'instructions'  => '' ,
		  'required'  => '0' ,
		  'default_value'  => '' ,
		  'key'  => $this->get_new_key() ,
		  'order_no'  => '0' ,
		);
		
		$array = array_merge($array, $args);

		return $array;
	}
	/**
	* get_default_datepicker_field()
	* @desc Returns an array of default data for creating a date picker field
	* @param array()
	* @return array()
	**/
	public function get_default_datepicker_field($args = array())
	{
		$this->active .= '_'.$args['name'];
		
		$array = array (
		  'label'  => $this->sanitize_lable($args['label']) ,
		  'name'  => $this->sanitize_name($args['name'], $this->sanitize_lable($args['label'])) ,
		  'type'  => 'date_picker' ,
		  'instructions'  => '' ,
		  'required'  => '0' ,
		  'date_format' => 'yymmdd', //yymmdd, read more at http://docs.jquery.com/UI/Datepicker/formatDate
		  'display_format' => 'yy-mm-dd', //yyyy-mm-dd, read more at http://docs.jquery.com/UI/Datepicker/formatDate
		  'key'  => $this->get_new_key() ,
		  'order_no'  => '0' ,
		);
		
		$array = array_merge($array, $args);

		return $array;
	}
	/**
	* get_default_rule()
	* @desc Returns an array of default data for creating a field group rule
	* @param array()
	* @return array()
	**/
	public function get_default_rule($args = array())
	{
		
		$array = array (
			'param'  => 'page_template' , // page_template
			'operator'  => '==' , // ==
			'value'  => '' , // template.php
		);
		
		if(empty($args) && sizeof($this->field_groups[$this->active]['location']['rules']) == 0)
		{
			return array();
		}
		
		$array = array_merge($array, $args);
		
		return $array;
	}
	/**
	* get_default_option()
	* @desc Returns an array of default data for creating a field group option
	* @param array()
	* @return array()
	**/
	public function get_default_options($args = array())
	{
		$array = array (
			'position'  => 'normal' , // normal, side
			'layout'  => 'default' ,
			'hide_on_screen'  => array (),
		);
		
		$array = array_merge($array, $args);
		$array['hide_on_screen'] = !empty($array['hide_on_screen']) ? $array['hide_on_screen'] : $this->get_default_hide_on_screen();
		return $array;
	}
	/**
	* get_default_hide_on_screen()
	* @desc Returns an array of default data for creating a field group hide on screen list
	* @param array()
	* @return array()
	**/
	public function get_default_hide_on_screen($args = array())
	{
		$array = array();
		
		$array = array_merge($array, $args);
		
		return $array;
	}
	/**
	* get_default_location()
	* @desc Returns an array of default data for creating field group location
	* @param array()
	* @return array()
	**/
	public function get_default_location($args = array())
	{
		$array = array (
			'rules' => array(),
			'allorany'  => 'any' ,
		);
		
		$array['rules'][sizeof($this->field_groups[$this->active]['location']['rules'])] = !empty($array['rules']) ? $array['rules'] : $this->get_default_rule();
		
		return $array;
	}
	/**
	* get_default_group()
	* @desc Returns an array of default data for creating a field group
	* @param array()
	* @return array()
	**/
	public function get_default_group($args = array())
	{
		$this->active .= '_group_'.$args['title'];
		
		$array = array (
			'id'  => $this->get_new_id() ,
			'title'  => '' ,
			'fields'  => array(),
			'location'  => array(),
			'options'  => array(),
			'menu_order'  => 0,
		);
		
		$array = array_merge($array, $args);
		
		$array['location'] = !empty($array['location']) ? $array['location'] : $this->get_default_location();
		$array['options'] = !empty($array['options']) ? $array['options'] : $this->get_default_options();
		
		return $array;
	}
	/**
	* add_field_group()
	* @desc Create a new field group
	* @param string
	* @param array() | string
	* @param array() | string 
	* @return boolean
	**/
	public function add_field_group($field_group_name = false, $args1 = false, $args2 = false)
	{
		
		if(is_array($args1))
		{
			$args = $args1;
		} 
		else if(is_array($args2))
		{
			$args = $args2;
		}
		else
		{
			$args = array();
		}
		if(is_string($args1))
		{
			$args['title'] = $args1;
		}
		if(!$field_group_name)
		{
			$field_group_name = 'fieldgroup_'.sizeof($this->field_groups);
		}
		
		if(empty($args['title']))
		{
			$args['title'] = $field_group_name;
		}
		
		$field_group_name = $this->sanitize_name(false, $field_group_name);
		$this->active = $field_group_name;
		
		$this->field_groups[$field_group_name] = $this->get_default_group($args);
		return true;
	}
	/**
	* update_location()
	* @desc Updates location in field group
	* @param string
	* @param array()
	* @return boolean
	**/
	public function update_location($field_group_name = false, $args = array())
	{
		if(!$field_group_name)
		{
			return false;
		}
		
		$this->active = $field_group_name;
		$this->field_groups[$field_group_name]['location'] = $this->get_default_location($args);
		return true;
	}
	/**
	* update_option()
	* @desc Updates options in field group
	* @param string
	* @param array()
	* @return boolean
	**/
	public function update_options($field_group_name = false, $args = array())
	{
		if(!$field_group_name)
		{
			return false;
		}
		
		$this->active = $field_group_name;
		$this->field_groups[$field_group_name]['options'] = $this->get_default_options($args);
		return true;
	}
	/**
	* add_hide_on_screen()
	* @desc Adds/updates hide on screen in field group
	* @param string
	* @param array()
	* @return boolean
	**/
	public function add_hide_on_screen($field_group_name = false, $args = array())
	{
		if(!$field_group_name)
		{
			return false;
		}
		
		$this->active = $field_group_name;
		$this->field_groups[$field_group_name]['options']['hide_on_screen'] = $this->get_default_hide_on_screen($args);
		return true;
	}
	/**
	* add_rule()
	* @desc Adds a new rule in field group
	* @param string
	* @param array()
	* @return boolean
	**/
	public function add_rule($field_group_name = false, $args = array())
	{
		if(!$field_group_name)
		{
			return false;
		}
		
		$this->active = $field_group_name;
		
		if(sizeof($this->field_groups[$this->active]['location']['rules']) > 0)
		{
			if(empty($this->field_groups[$field_group_name]['location']['rules'][sizeof($this->field_groups[$field_group_name]['location']['rules'])-1]))
			{
				unset($this->field_groups[$field_group_name]['location']['rules'][sizeof($this->field_groups[$field_group_name]['location']['rules'])-1]);
			}
		}
		$this->field_groups[$field_group_name]['location']['rules'][sizeof($this->field_groups[$field_group_name]['location']['rules'])] = $this->get_default_rule($args);
		return true;
	}
	/**
	* add_field()
	* @desc Adds a new unspecified field in field group
	* @param string
	* @param array()
	* @return boolean
	**/
	public function add_field($field_group_name = false, $args = array())
	{
		if(!$field_group_name)
		{
			return false;
		}
		
		$this->active = $field_group_name;
		$this->field_groups[$field_group_name]['fields'][sizeof($this->field_groups[$field_group_name]['fields'])] = $this->get_default_field($args);
		return true;
	}
	/**
	* add_text_field()
	* @desc Adds a new text field in field group
	* @param string
	* @param array()
	* @return boolean
	**/
	public function add_text_field($field_group_name = false, $args = array())
	{
		if(!$field_group_name)
		{
			return false;
		}
		
		$this->active = $field_group_name;
		$this->field_groups[$field_group_name]['fields'][sizeof($this->field_groups[$field_group_name]['fields'])] = $this->get_default_text_field($args);
		return true;
	}
	/**
	* add_textarea_field()
	* @desc Adds a new text field in field group
	* @param string
	* @param array()
	* @return boolean
	**/
	public function add_textarea_field($field_group_name = false, $args = array())
	{
		if(!$field_group_name)
		{
			return false;
		}
		
		$this->active = $field_group_name;
		$this->field_groups[$field_group_name]['fields'][sizeof($this->field_groups[$field_group_name]['fields'])] = $this->get_default_textarea_field($args);
		return true;
	}
	/**
	* add_colorpicker_field()
	* @desc Adds a new color picker field in field group
	* @param string
	* @param array()
	* @return boolean
	**/
	public function add_colorpicker_field($field_group_name = false, $args = array())
	{
		if(!$field_group_name)
		{
			return false;
		}
		
		$this->active = $field_group_name;
		$this->field_groups[$field_group_name]['fields'][sizeof($this->field_groups[$field_group_name]['fields'])] = $this->get_default_colorpicker_field($args);
		return true;
	}
	/**
	* add_datepicker_field()
	* @desc Adds a new date picker field in field group
	* @param string
	* @param array()
	* @return boolean
	**/
	public function add_datepicker_field($field_group_name = false, $args = array())
	{
		if(!$field_group_name)
		{
			return false;
		}
		
		$this->active = $field_group_name;
		$this->field_groups[$field_group_name]['fields'][sizeof($this->field_groups[$field_group_name]['fields'])] = $this->get_default_datepicker_field($args);
		return true;
	}
	/**
	* add_pagelink_field()
	* @desc Adds a new page link field in field group
	* @param string
	* @param array()
	* @return boolean
	**/
	public function add_pagelink_field($field_group_name = false, $args = array())
	{
		if(!$field_group_name)
		{
			return false;
		}
		
		$this->active = $field_group_name;
		$this->field_groups[$field_group_name]['fields'][sizeof($this->field_groups[$field_group_name]['fields'])] = $this->get_default_pagelink_field($args);
		return true;
	}
	/**
	* add_select_field()
	* @desc Adds a new select field in field group
	* @param string
	* @param array()
	* @return boolean
	**/
	public function add_select_field($field_group_name = false, $args = array())
	{
		if(!$field_group_name)
		{
			return false;
		}
		
		$this->active = $field_group_name;
		$this->field_groups[$field_group_name]['fields'][sizeof($this->field_groups[$field_group_name]['fields'])] = $this->get_default_select_field($args);
		return true;
	}
	
	/**
	* add_wysisyg_field()
	* @desc Adds a new wysiwyg field in field group
	* @param string
	* @param array()
	* @return boolean
	**/
	public function add_wysiwyg_field($field_group_name = false, $args = array())
	{
		if(!$field_group_name)
		{
			return false;
		}
		
		$this->active = $field_group_name;
		$this->field_groups[$field_group_name]['fields'][sizeof($this->field_groups[$field_group_name]['fields'])] = $this->get_default_wysiwyg_field($args);
		return true;
	}
	/**
	* add_image_field()
	* @desc Adds a new image field in field group
	* @param string
	* @param array()
	* @return boolean
	**/
	public function add_image_field($field_group_name = false, $args = array())
	{
		if(!$field_group_name)
		{
			return false;
		}
		
		$this->active = $field_group_name;
		$this->field_groups[$field_group_name]['fields'][sizeof($this->field_groups[$field_group_name]['fields'])] = $this->get_default_image_field($args);
		return true;
	}
	/**
	* get_new_key()
	* @desc Uses get_new_id() to generate a new field id or fetches current from db
	* @return string
	**/
	private function get_new_key()
	{
		return 'field_'.$this->get_new_id();
	}
	/**
	* get_new_key()
	* @desc Generates a new id or fetches current from db
	* @param int
	* @return string
	**/
	private function get_new_id($length = 12)
	{
		//Checks if a id exists
		if(empty($this->ids[$this->active]))
		{
			//...else we create one, saves it and returns it
			$alpha_numeric = 'abcdefghijklmnopqrstuvwxyz0123456789';
			$id = substr(str_shuffle($alpha_numeric), 0, $length);
			$this->ids[$this->active] = $id;
			
			return $id;
			
		} else {
			
			//...returns the existing id
			return $this->ids[$this->active];
			
		}
	}
	
	/**
	* Function aliases
	**/
	public function add_text($field_group_name = false, $args = array())
	{
		$this->add_text_field($field_group_name, $args);
	}
	public function add_image($field_group_name = false, $args = array())
	{
		$this->add_image_field($field_group_name, $args);
	}
	public function add_textarea($field_group_name = false, $args = array())
	{
		$this->add_textarea_field($field_group_name, $args);
	}
	public function add_wysiwyg($field_group_name = false, $args = array())
	{
		$this->add_wysiwyg_field($field_group_name, $args);
	}
	public function add_datepicker($field_group_name = false, $args = array())
	{
		$this->add_datepicker_field($field_group_name, $args);
	}
	public function add_colorpicker($field_group_name = false, $args = array())
	{
		$this->add_colorpicker_field($field_group_name, $args);
	}
	public function add_pagelist($field_group_name = false, $args = array())
	{
		$this->add_pagelist_field($field_group_name, $args);
	}
	public function add_select($field_group_name = false, $args = array())
	{
		$this->add_select_field($field_group_name, $args);
	}
}

//Define the ACFFC object globally
global $ACFFC;
$ACFFC = new ACFFC();

//Run the register function though action hook to allow both child and parent to register fields
add_action('init', 'register_acffc');
function register_acffc(){
	global $ACFFC;
	$ACFFC->register();
}
?>